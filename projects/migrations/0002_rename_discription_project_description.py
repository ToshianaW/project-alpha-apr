# Generated by Django 4.2 on 2023-04-24 21:40

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("projects", "0001_initial"),
    ]

    operations = [
        migrations.RenameField(
            model_name="project",
            old_name="discription",
            new_name="description",
        ),
    ]
